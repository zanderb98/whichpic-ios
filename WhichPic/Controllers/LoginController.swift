//
//  LoginController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/7/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKLoginKit
import GoogleSignIn

class LoginController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    static var googleSignInCallback: ((String, String, URL?) -> Void)?
    
    @IBOutlet weak var spinnerView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var googleCard: UIView!
    @IBOutlet weak var facebookCard: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        if  Util.getUser() != nil {
            let mainVC = storyboard!.instantiateViewController(withIdentifier: "containerVC")
            self.navigationController?.pushViewController(mainVC, animated: false)
        } else {
            super.viewWillAppear(animated)
        }
        
        background.backgroundColor = UIColor(patternImage: UIImage(named: "login_bg")!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        facebookCard.setCornerRadius(radius: 10)
        googleCard.setCornerRadius(radius: 10)
        
        facebookCard.addDropShadow()
        googleCard.addDropShadow()
    }
    
    @IBAction func facebookLoginPressed(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }

        let loginManager = LoginManager()
        loginManager.logIn(permissions: [ Permission.publicProfile.name ], from: self) { (result, err) in
            if let loginResult = result {
                if loginResult.isCancelled {
                    print("User cancelled login.")
                } else if loginResult.grantedPermissions.isEmpty {
                    print("Didnt grant perms")
                } else {
                    if let token = loginResult.token {
                        self.spinnerView.isHidden = false
                        AccountLoader.loginLoadFB(accessToken: token, complete: {
                            (firstLogin) in
                            
                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "containerVC")
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                            self.spinnerView.isHidden = true
                        }, failed: {
                            self.spinnerView.isHidden = true
                            Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at contact@whichpic.com.")
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func loginWithGoogle(_ sender: Any) {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        LoginController.googleSignInCallback = { idToken, accessToken, profilePicURL in
            
        }

        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("Error signing in with Google\(error.localizedDescription)")
            Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at contact@whichpic.com.")
            return
        }
        
        let idToken = user.authentication.idToken ?? ""
        let accessToken = user.authentication.accessToken ?? ""
        
        let imageWidth: CGFloat = UIScreen.main.bounds.width
        let pic = user.profile.imageURL(withDimension: UInt(round(imageWidth * UIScreen.main.scale)))
        
        self.spinnerView.isHidden = false
        
        AccountLoader.loginLoadGoogle(idToken: idToken, accessToken: accessToken, profilePicURL: pic, complete: {
            (firstLogin) in
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "containerVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
            self.spinnerView.isHidden = true
        }, failed: {
            self.spinnerView.isHidden = true
            Util.defaultAlert(self, title: "Login Failed", message: "Please try again. If this problem persists, please email our support team at contact@whichpic.com.")
        })
    }
}
