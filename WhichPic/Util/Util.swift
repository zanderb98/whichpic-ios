//
//  Util.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import MaterialComponents
import SwiftEntryKit

class Util {
    static func getUser() -> User? {
        let realm = Util.getRealm()
        return realm.objects(User.self).first
    }
    
    static func fadeOut(views: [UIView], duration: TimeInterval, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: {
            for view in views {
                view.alpha = 0
            }
        }) { (_) in
            for view in views {
                view.isHidden = true
            }
            
            if completion != nil {
                completion!()
            }
        }
    }

    static func fadeIn(views: [UIView], duration: TimeInterval, completion: (() -> Void)? = nil) {
        for view in views {
            view.alpha = 0
            view.isHidden = false
        }
        
        UIView.animate(withDuration: duration, animations: {
            for view in views {
                view.alpha = 1
            }
        }) { (_) in
            if completion != nil {
                completion!()
            }
        }
    }
    
    static func imageWith(name: String?) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let nameLabel = UILabel(frame: frame)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = Colors.main
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 30)
        nameLabel.text = name
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
    
    static func defaultAlert(_ vc: UIViewController, title: String, message: String? = nil, hasCancel: Bool = false, action: (() -> Void)? = nil) {
        
        var attributes = EKAttributes.centerFloat
        
        attributes.roundCorners = .all(radius: 20)
        attributes.shadow = .active(with: .init(opacity: 0.3, radius: 5))
        // Set its background to white
        attributes.entryBackground = .color(color: .white)
        attributes.screenBackground = .color(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.4))
        attributes.screenInteraction = .dismiss
        
        // Animate in and out using default translation
        attributes.entranceAnimation = .translation
        attributes.exitAnimation = .translation
        
        attributes.displayDuration = .infinity
        attributes.entryInteraction = .forward
        
        let alert = WhichPicAlert()
        alert.setup(title: title, message: message, hasCancel: hasCancel)
        
        alert.cancelPressed = {
            SwiftEntryKit.dismiss()
        }
        
        alert.okPressed = {
            () in
            
            SwiftEntryKit.dismiss()
            action?()
        }
        
        SwiftEntryKit.display(entry: alert, using: attributes)
    }
    
    static func instanceFromNib(nibName: String) -> UIView {
        return UINib(nibName: nibName, bundle: .main).instantiate(withOwner: nil, options: nil)[0] as! UIView
        
    }
    
    static func getRealm() -> Realm {
        let realm = try! Realm()
        return realm
    }

}
