//
//  ContainerController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/26/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class ContainerController: UIViewController {
    
    static var instance: ContainerController?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var voteButton: UIView!
    @IBOutlet weak var voteFiller: UIView!
    @IBOutlet weak var voteImage: UIImageView!
    @IBOutlet weak var picsFilled: UIImageView!
    @IBOutlet weak var picsButton: UIImageView!
    @IBOutlet weak var accountFilled: UIImageView!
    @IBOutlet weak var accountButton: UIImageView!
    @IBOutlet weak var karmaBack: UIView!
    @IBOutlet weak var karmaFront: UIView!
    @IBOutlet weak var bottomGradient: GradientView!
    
    var readyToVote = false
    
    var voteVC: VoteController!
    var picsVC: PicsController!
    var accountVC: AccountController!
    
    var currController: UIViewController!
    
    var currPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ContainerController.instance = self
                
        navigationController?.hero.isEnabled = true
        
        voteVC = (storyboard!.instantiateViewController(withIdentifier: "voteVC") as! VoteController)
        picsVC = (storyboard!.instantiateViewController(withIdentifier: "picsVC") as! PicsController)
        accountVC = (storyboard!.instantiateViewController(withIdentifier: "accountVC") as! AccountController)
        
        currController = voteVC
        
        self.addChild(voteVC)
        voteVC.view.frame = containerView.bounds
        containerView.addSubview(voteVC.view)
        voteVC.didMove(toParent: self)

        voteButton.layer.borderColor = Colors.main.cgColor
        voteButton.layer.borderWidth = 2
        
        voteButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(votePressed)))
        
        karmaBack.setCornerRadius(radius: karmaBack.bounds.height / 2)
        karmaFront.setCornerRadius(radius: karmaFront.bounds.height / 2)
        
        if let user = Util.getUser() {
            karmaFront.getConstraintByID(id: "width")?.constant = karmaBack.bounds.width * user.karma
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.fillVoteButton(2)
        }
    }
    
    func karmaUpdated() {
        if let user = Util.getUser() {
            karmaFront.getConstraintByID(id: "width")?.constant = karmaBack.bounds.width * user.karma
            accountVC.karmaUpdated(karma: user.karma)
        }
    }
    
    @objc func votePressed() {
        if currPage == 1 {
            if readyToVote {
                readyToVote = false
                
                if voteVC.hasPics() {
                    voteVC.votePressed()
                    fillVoteButton(2)
                }
                
            }
        } else {
            readyToVote = false
            
            UIView.animate(withDuration: 0.3) {
                self.voteButton.alpha = 1
                self.voteFiller.alpha = 1
                
                self.karmaBack.alpha = 1
                self.karmaFront.alpha = 1
            }
            
            fillVoteButton(2)
            
            Util.fadeOut(views: [currPage == 0 ? picsFilled : accountFilled], duration: 0.3)
            
            self.titleLabel.text = "whichpic?"
            
            switchToController(newPage: 1, newController: voteVC)
            currPage = 1
        }
    }
    
    @IBAction func picsPressed(_ sender: Any) {
        if currPage == 0 { return }
        
        if currPage == 1 {
            emptyVoteButton(0.3)
            
            UIView.animate(withDuration: 0.3) {
                self.voteButton.alpha = 0.55
                self.voteFiller.alpha = 0.55
            }
        } else {
            Util.fadeOut(views: [accountFilled], duration: 0.3)
        }
        
        self.titleLabel.text = "my pics"
        
        UIView.animate(withDuration: 0.3) {
            self.karmaBack.alpha = 1
            self.karmaFront.alpha = 1
        }
        
        Util.fadeIn(views: [picsFilled], duration: 0.3)
        switchToController(newPage: 0, newController: picsVC)
        currPage = 0
    }

    @IBAction func accountPressed(_ sender: Any) {
        if currPage == 2 { return }
        
        if currPage == 1 {
            emptyVoteButton(0.3)
            
            UIView.animate(withDuration: 0.3) {
                self.voteButton.alpha = 0.55
                self.voteFiller.alpha = 0.55
            }
        } else {
            Util.fadeOut(views: [picsFilled], duration: 0.3)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.karmaBack.alpha = 0
            self.karmaFront.alpha = 0
        }
        
        self.titleLabel.text = "account"
        
        Util.fadeIn(views: [accountFilled], duration: 0.3)
        switchToController(newPage: 2, newController: accountVC)
        currPage = 2
    }
    
    private func fillVoteButton(_ duration: Double) {
        voteFiller.getConstraintByID(id: "fillerHeight")?.constant = 0
        voteButton.layoutIfNeeded()
        
        voteFiller.getConstraintByID(id: "fillerHeight")?.constant = voteButton.frame.height
     
        UIView.animate(withDuration: duration, animations: {
            self.voteButton.layoutIfNeeded()
        }) { (success) in
            if success {
                self.readyToVote = true
            }
        }
    }
    
    private func emptyVoteButton(_ duration: Double) {
        voteFiller.getConstraintByID(id: "fillerHeight")?.constant = 0
        
        UIView.animate(withDuration: duration, animations: {
            self.voteButton.layoutIfNeeded()
        })
    }
        
    func switchToController(newPage: Int, newController: UIViewController) {
        let width = self.view.frame.width
        
        self.addChild(newController)
        newController.view.frame = CGRect(x: newPage < currPage ? -width : width, y: 0, width: width, height: currController.view.frame.height)
        containerView.addSubview(newController.view)
        newController.didMove(toParent: self)

        UIView.animate(withDuration: 0.3, animations: {
            newController.view.frame.origin.x = 0
            self.currController.view.frame.origin.x = newPage < self.currPage ? width : -width
            
            self.bottomGradient.alpha = newPage == 0 ? 1 : 0
        }) { (_) in
            self.currController.willMove(toParent: nil)
            self.currController.view.removeFromSuperview()
            self.currController.removeFromParent()
            
            self.currController = newController
        }
    }

}
