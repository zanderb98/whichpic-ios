//
//  AccountController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/27/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import GoogleSignIn

class AccountController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var karmaContainer: UIView!
    @IBOutlet weak var genderContainer: UIView!
    @IBOutlet weak var voteOnContainer: UIView!
    @IBOutlet weak var contactContainer: UIView!
    @IBOutlet weak var karmaBack: UIView!
    @IBOutlet weak var karmaFront: UIView!
    @IBOutlet weak var genderPicker: WhichPicPickerView!
    @IBOutlet weak var voteOnPicker: WhichPicPickerView!
    
    @IBOutlet weak var logoutButton: UIView!
    
    var karma: CGFloat
        = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        profileImage.addDropShadow(offset: CGSize(width: 0, height: -5))
        card.setCornerRadius(radius: 40)
        card.addDropShadow()
       
        karmaContainer.setCornerRadius(radius: 10)
        genderContainer.setCornerRadius(radius: 10)
        voteOnContainer.setCornerRadius(radius: 10)
        contactContainer.setCornerRadius(radius: 10)
        
        karmaBack.setCornerRadius(radius: karmaBack.bounds.height / 2)
        karmaFront.setCornerRadius(radius: karmaFront.bounds.height / 2)
        
        karmaFront.addDropShadow()
        
        genderPicker.setCornerRadius(radius: 15)
        voteOnPicker.setCornerRadius(radius: 15)
        
        genderPicker.setup(choices: ["male", "female", "other"], selected: 0)
        voteOnPicker.setup(choices: ["male", "female", "all"], selected: 1)
        
        logoutButton.setCornerRadius(radius: 10)
        logoutButton.getConstraintByID(id: "topSpace")?.constant = -1 * logoutButton.frame.height / 2 - 8
        logoutButton.addDropShadow()
        
        view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let user = Util.getUser() {
            profileImage.image = user.image ?? UIImage(named: "empty_profile_picture")
            nameLabel.text = "\(user.name)"
            
            genderPicker.select(choice: genderPicker.choices.firstIndex(of: user.gender) ?? 2)
            voteOnPicker.select(choice: voteOnPicker.choices.firstIndex(of: user.voteOn) ?? 2)
            
            self.karma = user.karma
        }
        
        karmaFront.getConstraintByID(id: "width")?.constant = karmaBack.bounds.width * karma
        
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    }
    
    func karmaUpdated(karma: CGFloat) {
         self.karma = karma
        
        if karmaFront == nil || karmaBack == nil {
           return
        }
        
        karmaFront.getConstraintByID(id: "width")?.constant = karmaBack.bounds.width * karma
    }

    @IBAction func logoutPressed(_ sender: Any) {
        if let loginVC = self.parent?.storyboard?.instantiateViewController(withIdentifier: "loginVC") {
            let realm = Util.getRealm()
            
            realm.beginWrite()
            realm.deleteAll()
            try! realm.commitWrite()
            
            GIDSignIn.sharedInstance().signOut()
            
            self.parent?.navigationController?.viewControllers.insert(loginVC, at: 0)
            self.parent?.navigationController?.popToViewController(loginVC, animated: true)
            self.parent?.navigationController?.viewControllers = [loginVC]
        }
    }
}
