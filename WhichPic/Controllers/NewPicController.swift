//
//  NewPicController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/10/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class NewPicController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var spinnerView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var bottomCard: UIView!
    @IBOutlet weak var addImageView: UIView!
    @IBOutlet weak var notesCollection: UICollectionView!
    @IBOutlet weak var votedOnByContainer: UIView!
    @IBOutlet weak var votedOnByPicker: WhichPicPickerView!
    @IBOutlet weak var uploadButton: UIView!
    
    @IBOutlet weak var karmaTest: TestTypeView!
    @IBOutlet weak var roughTest: TestTypeView!
    @IBOutlet weak var standardTest: TestTypeView!
    @IBOutlet weak var preciseTest: TestTypeView!
    @IBOutlet weak var imageButtonHeight: NSLayoutConstraint!
    
    var newPicAdded: ((Pic) -> Void)?
    
    var notes = ["person on the left", "person on the right", "person in the middle", "custom +"]
    var tests: [TestTypeView] = []
    var selectedType = 0
    var selectedNotes: [NotesCell] = []
    
    var noteCells: [IndexPath: NotesCell] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomCard.addDropShadow()
        bottomCard.setCornerRadius(radius: 40)
        
        backButton.setCornerRadius(radius: backButton.frame.height / 2)
        backButton.addDropShadow()
        
        uploadButton.setCornerRadius(radius: 40)
        uploadButton.addDropShadow()
        
        var selected = 2
        
        if let user = Util.getUser() {
            if user.gender == "male" {
                selected = 1
            } else if user.gender == "female" {
                selected = 0
            }
        }
        
        tests = [karmaTest, roughTest, standardTest, preciseTest]
        
        karmaTest.setup(testName: "Karma Test", priceStr: "Free", desc: Config.karmaTestDesc)
        roughTest.setup(testName: "Rough Test", priceStr: "$1.99", desc: Config.roughTestDesc)
        standardTest.setup(testName: "Standard Test", priceStr: "$4.99", desc: Config.standardTestDesc)
        preciseTest.setup(testName: "Precise Test", priceStr: "$9.99", desc: Config.preciseTestDesc)
        
        karmaTest.setSelected(isSelected: true, duration: 0)
        
        karmaTest.pressedCallback = {
            self.tests[self.selectedType].setSelected(isSelected: false)
            self.selectedType = 0
            self.karmaTest.setSelected(isSelected: true)
        }
        
        roughTest.pressedCallback = {
            self.tests[self.selectedType].setSelected(isSelected: false)
            self.selectedType = 1
            self.roughTest.setSelected(isSelected: true)
        }
        
        standardTest.pressedCallback = {
            self.tests[self.selectedType].setSelected(isSelected: false)
            self.selectedType = 2
            self.standardTest.setSelected(isSelected: true)
        }
        
        preciseTest.pressedCallback = {
            self.tests[self.selectedType].setSelected(isSelected: false)
            self.selectedType = 3
            self.preciseTest.setSelected(isSelected: true)
        }
        
        votedOnByPicker.setup(choices: ["male", "female", "all"], selected: selected)
        votedOnByContainer.setCornerRadius(radius: 10)
        
        let flowLayout = LeftAlignedCollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        notesCollection.collectionViewLayout = flowLayout
        notesCollection.dataSource = self
        notesCollection.register(UINib(nibName: "NotesCell", bundle: nil), forCellWithReuseIdentifier: "NotesCell")

    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addImagePressed() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.mediaTypes = ["public.image"]
        pickerController.sourceType = .photoLibrary
        
        self.navigationController?.present(pickerController, animated: true)
    }
    
    var toSelect: [String] = []
    
    func addNote(_ newNote: String) {
        let index = notes.count - 1
        
        notes.insert(newNote, at: index)
        
        toSelect.append(newNote)
        notesCollection.insertItems(at: [IndexPath(row: index, section: 0)])
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.viewDidLayoutSubviews()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let img = info[.originalImage] as? UIImage else {
           return
        }
        
        self.image.image = img
        addImageView.isHidden = true
        
        let imgHeight = img.size.height * (image.frame.width / img.size.width)
        
        self.image.getConstraintByID(id: "height")?.constant = imgHeight
        self.imageButtonHeight.constant = imgHeight
        
        self.view.layoutIfNeeded()
        
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.notesCollection.getConstraintByID(id: "collectionHeight")?.constant = notesCollection.collectionViewLayout.collectionViewContentSize.height
        
        self.view.layoutIfNeeded()
    }
}

extension NewPicController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotesCell", for: indexPath) as! NotesCell
        
        cell.setup(text: notes[indexPath.row], vc: self)
        
        if toSelect.contains(notes[indexPath.row]) {
            cell.pressed(cell)
            toSelect.remove(at: toSelect.firstIndex(of: notes[indexPath.row])!)
        }
        
        noteCells[indexPath] = cell
        return cell
    }
    
    @IBAction func uploadPressed(sender: Any?) {
        guard let img = image.image else {
            Util.defaultAlert(self, title: "Oops...", message: "Please upload an image")
            return
        }
        
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        spinnerView.isHidden = false
        
        var notes: [String] = []
        
        for cell in selectedNotes {
            notes.append(cell.note.text!)
        }
        
        PicManager.uploadPic(img, votedOnBy: votedOnByPicker.choices[votedOnByPicker.selected],
                             notes: notes, testType: "karma", callback: { (pic) in
                                self.spinnerView.isHidden = true
                                
                                if pic != nil {
                                    Util.defaultAlert(self, title: "Success!", message: "Pic uploaded successfully", action: {
                                        self.navigationController?.popViewController(animated: true)
                                        
                                        self.newPicAdded?(pic!)
                                    })
                                } else {
                                    Util.defaultAlert(self, title: "Oops...", message: "Failed to upload pic, please try again")
                                }
        })
    }
    
}
