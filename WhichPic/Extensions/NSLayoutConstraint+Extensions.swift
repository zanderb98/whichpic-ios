//
//  NSLayoutConstraint+Extensions.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/1/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
