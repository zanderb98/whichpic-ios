//
//  AppDelegate.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleSignIn
import FacebookCore
import FBSDKCoreKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var timer: Timer?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GIDSignIn.sharedInstance().clientID = Config.shared.googleClientID
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        pauseUpdates()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        pauseUpdates()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        startUpdates()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        startUpdates()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        pauseUpdates()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == Config.shared.googleURLScheme {
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[.sourceApplication] as? String,
                                                     annotation: options[.annotation])
        } else {
            return ApplicationDelegate.shared.application(app,
                                                         open: url,
                                                         sourceApplication: options[.sourceApplication] as? String,
                                                         annotation: options[.annotation] as Any)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenStr = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        if let user = Util.getUser() {
            let params = [
                "UserID": user.id,
                "Platform": "iOS",
                "DeviceToken": tokenStr,
                "ServerVersion": "2"
            ]
            
            Alamofire.request(Config.shared.registerDeviceURL, method: .post, parameters: params)
        }
        
        UserDefaults.standard.set(tokenStr, forKey: "DeviceToken")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for PUSH with error: \(error)")
    }
    
    //Update user every 15 seconds
    func startUpdates() {
        timer = Timer.scheduledTimer(withTimeInterval: 15, repeats: true, block: { (_) in
            guard let user = Util.getUser() else {
                return
            }
            
            UserManager.updateUser(user)
        })
    }
    
    func pauseUpdates() {
        if timer == nil { return }
        
        timer!.invalidate()
        timer = nil
    }

}

