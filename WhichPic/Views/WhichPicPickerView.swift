//
//  WhichPicPickerView.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/5/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class WhichPicPickerView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var choice1: UIView!
    @IBOutlet weak var choice2: UIView!
    @IBOutlet weak var choice3: UIView!

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    var selected: Int = 0
    var choices: [String] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("WhichPicPickerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    func setup(choices: [String], selected: Int) {
        clipsToBounds = false
        
        choice1.setCornerRadius(radius: 10)
        choice2.setCornerRadius(radius: 10)
        choice3.setCornerRadius(radius: 10)
        
        choice1.layer.borderWidth = 0.7
        choice2.layer.borderWidth = 0.7
        choice3.layer.borderWidth = 0.7
        
        if choices.count != 3 {
            return
        }
        
        self.selected = selected
        self.choices = choices
        
        label1.text = choices[0]
        label2.text = choices[1]
        label3.text = choices[2]
        
        self.select(choice: selected)
    }
    
    
    func select(choice: Int) {
        selected = choice
        
        var selectedView = choice1
        
        if choice == 1 {
            selectedView = choice2
        } else if choice == 2 {
            selectedView = choice3
        }
        
        if let sView = selectedView {
            contentView.bringSubviewToFront(sView)
        }
        
        UIView.animate(withDuration: 0.1) {
            self.choice1.addDropShadow(opacity: choice == 0 ? 0.3 : 0)
            self.choice2.addDropShadow(opacity: choice == 1 ? 0.3 : 0)
            self.choice3.addDropShadow(opacity: choice == 2 ? 0.3 : 0)
            
            let unselectedBG = UIColor(hex: "F2F2F2")
            
            self.choice1.backgroundColor = choice == 0 ? Colors.main : unselectedBG
            self.choice2.backgroundColor = choice == 1 ? Colors.main : unselectedBG
            self.choice3.backgroundColor = choice == 2 ? Colors.main : unselectedBG
            
            let unselectedText = UIColor(hex: "BBBBBB")
            
            self.label1.textColor = choice == 0 ? UIColor.white : unselectedText
            self.label2.textColor = choice == 1 ? UIColor.white : unselectedText
            self.label3.textColor = choice == 2 ? UIColor.white : unselectedText
            
            let borderColor = UIColor(hex: "BBBBBB")
            
            self.choice1.layer.borderColor = choice == 0 ? Colors.main.cgColor : borderColor.cgColor
            self.choice2.layer.borderColor = choice == 1 ? Colors.main.cgColor : borderColor.cgColor
            self.choice3.layer.borderColor = choice == 2 ? Colors.main.cgColor : borderColor.cgColor
        }
    }
    
    override var bounds: CGRect {
        didSet {
            setup(choices: choices, selected: selected)
        }
    }
    
    @IBAction func choice1Pressed(_ sender: Any) {
        self.select(choice: 0)
    }
    
    @IBAction func choice2Pressed(_ sender: Any) {
        self.select(choice: 1)
    }
    
    @IBAction func choice3Pressed(_ sender: Any) {
        self.select(choice: 2)
    }
}
