//
//  WhichPicAlert.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/15/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class WhichPicAlert: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var centerBar: UIView!
    @IBOutlet weak var cancelButton: UIView!
    @IBOutlet weak var cancelWidth: NSLayoutConstraint!
    
    var cancelPressed: (() -> Void)?
    var okPressed: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("WhichPicAlert", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        contentView.addConstraint(NSLayoutConstraint(item: contentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width - 48))
    }
    
    func setup(title: String, message: String? = nil, hasCancel: Bool = false) {
        titleLabel.text = title
        messageLabel.text = message
        
        if message == nil {
            messageLabel.addConstraint(NSLayoutConstraint(item: messageLabel as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0))
        }
        
        cancelButton.isHidden = !hasCancel
        centerBar.isHidden = !hasCancel
        cancelWidth.constant = hasCancel ? contentView.bounds.width / 2 : 0
        
        layoutIfNeeded()
    }
    
    @IBAction func cancelled(_ sender: Any) {
        cancelPressed?()
    }
    
    @IBAction func done(_ sender: Any) {
        okPressed?()
    }
    
}

