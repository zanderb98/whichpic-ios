//
//  TextInputDialog.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/14/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class TextInputDialog: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var cancelPressed: (() -> Void)?
    var donePressed: ((String?) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("TextInputDialog", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
//        contentView.setCornerRadius(radius: 40)
        
    }
    
    func setup(title: String, placeholder: String) {
        titleLabel.text = title
        textField.placeholder = placeholder
    }
    
    @IBAction func cancelled(_ sender: Any) {
        cancelPressed?()
    }
    
    @IBAction func done(_ sender: Any) {
        donePressed?(textField.text)
    }
    
}

