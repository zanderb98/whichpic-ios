//
//  Test.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class Pic: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var createdAt: Date?
    @objc dynamic var votedOnBy: String = ""
    @objc dynamic var testType: String = ""
    @objc dynamic var imageURL: String = ""
    @objc dynamic var data: Data? = nil
    @objc dynamic var score = CGFloat(Float(arc4random()) / Float(UINT32_MAX))

    var votes = List<Vote>()
    var notes = List<Note>()
    
    func setup(json: [String: Any], img: UIImage? = nil) {
        if let i = img {
            setImage(image: i)
        }
        
        id = (json["_id"] as? String) ?? ""
        votedOnBy = (json["votedOnBy"] as? String) ?? ""
        testType = (json["testType"] as? String) ?? ""
        score = (json["score"] as? CGFloat) ?? 0
        imageURL = (json["imageURL"] as? String) ?? ""
        createdAt = Date(timeIntervalSince1970: (json["createdAt"] as? Double) ?? 0)
        
        if let votesArr = json["votes"] as? [[String: Any]] {
            votes = List<Vote>()
            
            for voteJson in votesArr {
                let vote = Vote()
                vote.setup(json: voteJson)
                votes.append(vote)
            }
        }
        
        if let notesArr = json["notes"] as? [String] {
            notes = List<Note>()
            
            for str in notesArr {
                let note = Note()
                note.note = str
                notes.append(note)
            }
        }
    }
    
    func getVoteNotes() -> [String] {
        var ret: [String] = []
        
        for vote in votes {
            for note in vote.notes {
                ret.append(note.note)
            }
        }
        
        return ret
    }

    
    var image: UIImage? {
        return data == nil ? nil : UIImage(data: data!)
    }
    
    func setImage(image: UIImage) {
        data = image.pngData()
    }
    
    func downloadImage() {
        if imageURL.isEmpty { return }
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
