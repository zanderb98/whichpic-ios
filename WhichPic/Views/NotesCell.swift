//
//  NotesCell.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/26/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import SwiftEntryKit

class NotesCell: UICollectionViewCell {
    
    var selectable = true
    var isVoteNote = false

    @IBOutlet weak var note: UILabel!
    var is_selected = false
    var vc: UIViewController?
    
    var unselectedColor = UIColor(hex: "#AAAAAA")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]

        note.setCornerRadius(radius: 10)
        note.textAlignment = .center
        note.layer.borderColor = unselectedColor.cgColor
        note.layer.borderWidth = 1.0;
        
    }
    
    func setup(text: String, vc: UIViewController) {
        self.vc = vc
        
        note.text = text
        
        contentView.transform = CGAffineTransform(scaleX: isVoteNote ? -1 : 1, y: 1)

        note.layer.borderColor = isVoteNote ? UIColor.white.cgColor : unselectedColor.cgColor
        note.backgroundColor = isVoteNote ? UIColor(red: 0, green: 0, blue: 0, alpha: 0.45) : UIColor.clear
        note.textColor = isVoteNote ? UIColor.white : unselectedColor
        note.clipsToBounds = isVoteNote
        
        let width = getTextWidth(text: text)
        note.getConstraintByID(id: "NoteWidth")?.constant = width + 12
        
        layoutIfNeeded()
    }
    
    @IBAction func pressed(_ sender: Any) {
        if !selectable { return }
        is_selected = !is_selected
        
        let voteVC = vc as? VoteController
        let newPicVC = vc as? NewPicController
        
        if note.text == "custom +" && newPicVC != nil {
            var attributes = EKAttributes.centerFloat
            
            attributes.roundCorners = .all(radius: 20)
            attributes.shadow = .active(with: .init(opacity: 0.3, radius: 5))
            // Set its background to white
            attributes.entryBackground = .color(color: .white)
            attributes.screenBackground = .color(color: UIColor(red: 0, green: 0, blue: 0, alpha: 0.4))
            attributes.screenInteraction = .dismiss
            
            // Animate in and out using default translation
            attributes.entranceAnimation = .translation
            attributes.exitAnimation = .translation
            
            attributes.displayDuration = .infinity
            attributes.entryInteraction = .forward
            
            let dialog = TextInputDialog()
            dialog.setup(title: "Enter your custom note", placeholder: "Note text")
            
            dialog.cancelPressed = {
                SwiftEntryKit.dismiss()
            }
            
            dialog.donePressed = {
                (strIn) in
                
                SwiftEntryKit.dismiss()
                if let str = strIn {
                    if !str.isEmpty {
                        newPicVC!.addNote(str.lowercased())
                    }
                }
            }
            
            SwiftEntryKit.display(entry: dialog, using: attributes)
            return
        }
        
        if voteVC == nil && newPicVC == nil { return }
        
        if is_selected {
            if voteVC != nil { voteVC!.selectedNotes.append(self) }
            else { newPicVC!.selectedNotes.append(self) }
        } else {
            if voteVC != nil {  voteVC!.selectedNotes = voteVC!.selectedNotes.filter {$0 != self} }
            else { newPicVC!.selectedNotes = newPicVC!.selectedNotes.filter {$0 != self} }

        }
        
        let color = is_selected ? Colors.main : unselectedColor
        
        UIView.animate(withDuration: 0.3) {
            self.note.layer.borderColor = color.cgColor
            self.note.textColor = color
        }
    }
    
    func deselect() {
        UIView.animate(withDuration: 0.3) {
            self.note.layer.borderColor = self.unselectedColor.cgColor
            self.note.textColor = self.unselectedColor
        }
    }
    
    private func getTextWidth(text: String) -> CGFloat {
        let l = UILabel()
        l.font = note.font;
        l.text = text
        l.sizeToFit()
        
        return l.frame.width
    }

}
