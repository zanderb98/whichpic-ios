//
//  MyPicController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/1/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import Charts

class MyPicController: UIViewController, UICollectionViewDataSource {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var backButton: UIView!
    @IBOutlet weak var bottomCard: UIView!
    var pic: Pic?
    
    @IBOutlet weak var numVotes: UILabel!
    @IBOutlet weak var scoreBarBack: UIView!
    @IBOutlet weak var scoreBarFront: UIView!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var scatterChart: ScatterChartView!
    @IBOutlet weak var notesCollection: UICollectionView!
    
    var notes: [(note: String, amt: Int)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let p = pic, let img = p.image {
            image.image = img
            image.getConstraintByID(id: "height")?.constant = img.size.height * (image.frame.width / img.size.width)
            
            setup(with: p)
        }
        
        backButton.addDropShadow()
        backButton.setCornerRadius(radius: backButton.frame.width / 2)
        
        bottomCard.setCornerRadius(radius: 40)
        bottomCard.addDropShadow()
        
        scoreBarBack.setCornerRadius(radius: scoreBarBack.frame.height / 2)
        scoreBarFront.setCornerRadius(radius: scoreBarBack.frame.height / 2)
        
        let flowLayout = LeftAlignedCollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        notesCollection.collectionViewLayout = flowLayout
        notesCollection.dataSource = self
        notesCollection.register(UINib(nibName: "NotesCell", bundle: nil), forCellWithReuseIdentifier: "NotesCell")
    
    }
    
    func setup(with: Pic) {
        let p = with
        
        scoreBarFront.getConstraintByID(id: "width")?.constant = scoreBarBack.frame.width * p.score / 10.0
        
        let formatter = NumberFormatter()
        formatter.maximumIntegerDigits = 2
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        
        score.text = formatter.string(from: (p.score) as NSNumber) ?? ""
        numVotes.attributedText = NSMutableAttributedString().with("\(p.votes.count)", font:
            Fonts.sofiaSemibold(size: 28)).with(" votes", font: Fonts.sofiaLight(size: 28))
        
        var entries: [ChartDataEntry] = []
        var entriesDict: [Int: Int] = [:]
        
        for i in 0...100 {
            entriesDict[i] = 0
        }
        
        for vote in p.votes {
            entriesDict[Int(vote.score * 10)]! += 1
        }
        
        var maxVotes = 0
        
        for (score, votes) in entriesDict {
            if votes > 0 {
                for i in 1...votes {
                    entries.append(ChartDataEntry(x: Double(score) / 10.0, y: Double(i)))
                    maxVotes = max(votes, maxVotes)
                }
            }
        }
        
        let chartDataSet = MyScatterChartDataSet(entries, chartHeight: scatterChart.frame.height - CGFloat(20), numVals: max(3, maxVotes))
        chartDataSet.colors = [NSUIColor(cgColor: Colors.main.cgColor)]
        chartDataSet.valueFormatter = ChartValueFormatter()
        
        let chartData = ScatterChartData(dataSet: chartDataSet)
        
        scatterChart.legend.enabled = false
        scatterChart.noDataText = ""
        
        scatterChart.xAxis.drawGridLinesEnabled = false
        scatterChart.leftAxis.drawGridLinesEnabled = false
        
        scatterChart.scaleYEnabled = false
        scatterChart.scaleXEnabled = false
        scatterChart.pinchZoomEnabled = false
        scatterChart.doubleTapToZoomEnabled = false
        
        scatterChart.xAxis.axisLineColor = UIColor(hex: "bbbbbb")
        scatterChart.xAxis.labelTextColor = UIColor(hex: "666666")
        
        scatterChart.leftAxis.axisLineColor = UIColor(hex: "bbbbbb")
        scatterChart.leftAxis.labelTextColor = UIColor(hex: "666666")

        scatterChart.leftAxis.axisMinimum = 0
        scatterChart.leftAxis.axisMaximum = Double(max(maxVotes, 3))
        scatterChart.leftAxis.setLabelCount(min(5, max(maxVotes, 3)), force: false)
        scatterChart.xAxis.valueFormatter = MyXAxisValueFormatter()
        
        scatterChart.xAxis.granularityEnabled = true
        scatterChart.xAxis.granularity = 1
        
        scatterChart.rightAxis.enabled = false
        scatterChart.xAxis.labelPosition = .bottom
        
        scatterChart.xAxis.drawLabelsEnabled = true
        scatterChart.xAxis.labelFont = Fonts.sofiaSemibold(size: 10)
        scatterChart.leftAxis.labelFont = Fonts.sofiaSemibold(size: 10)
        
        var notesDict: [String: Int] = [:]
        
        for note in p.getVoteNotes() {
            notesDict[note] = (notesDict[note] ?? 0) + 1
        }
        
        notes = []
        for noteStr in Config.notes {
            if notesDict[noteStr] != nil {
                notes.append((note: noteStr, amt: notesDict[noteStr]!))
            }
        }
        
        notesCollection.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.scatterChart.data = chartData
            self.scatterChart.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotesCell", for: indexPath) as! NotesCell
        let note = notes[indexPath.row]
        cell.setup(text: "\(note.note)\(note.amt == 1 ? "" : " x\(note.amt)")", vc: self)
        cell.selectable = false
        return cell
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        notesCollection.getConstraintByID(id: "collectionHeight")?.constant = 200
    }
    
}

class ChartValueFormatter: NSObject, IValueFormatter {
    fileprivate var numberFormatter: NumberFormatter?
    
    convenience init(numberFormatter: NumberFormatter) {
        self.init()
        self.numberFormatter = NumberFormatter()
        numberFormatter.maximumIntegerDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.minimumFractionDigits = 1
    }
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        guard let numberFormatter = numberFormatter
            else {
                return ""
        }
        return numberFormatter.string(for: value)!
    }
}

class MyScatterChartDataSet: IScatterChartDataSet {
    
    var entries: [ChartDataEntry] = []
    var mainColor = NSUIColor(cgColor: Colors.main.cgColor)
    
    init(_ entries: [ChartDataEntry], chartHeight: CGFloat, numVals: Int) {
        self.entries = entries
        self.entryCount = entries.count
        
        self.shapeRenderer = MyShapeRenderer(chartHeight: chartHeight, numVals: numVals)
    }
    
    var scatterShapeSize: CGFloat = 2
    
    var scatterShapeHoleRadius: CGFloat = 0
    
    var scatterShapeHoleColor: NSUIColor?
    
    var shapeRenderer: IShapeRenderer?
    
    var drawHorizontalHighlightIndicatorEnabled: Bool = false
    
    var drawVerticalHighlightIndicatorEnabled: Bool = false
    
    var isHorizontalHighlightIndicatorEnabled: Bool = false
    
    var isVerticalHighlightIndicatorEnabled: Bool = false
    
    func setDrawHighlightIndicators(_ enabled: Bool) {
        self.drawHorizontalHighlightIndicatorEnabled = false
    }
    
    var highlightColor: NSUIColor = NSUIColor(cgColor: Colors.main.cgColor)
    
    var highlightLineWidth: CGFloat = 0
    
    var highlightLineDashPhase: CGFloat = 0
    
    var highlightLineDashLengths: [CGFloat]?
    
    func notifyDataSetChanged() {
        
    }
    
    func calcMinMax() {
    }
    
    func calcMinMaxY(fromX: Double, toX: Double) {
        
    }
    
    var yMin: Double = 0
    
    var yMax: Double = 3
    
    var xMin: Double = 0
    
    var xMax: Double = 10
    
    var entryCount: Int = 0
    
    func entryForIndex(_ i: Int) -> ChartDataEntry? {
        return entries[i]
    }
    
    func entryForXValue(_ xValue: Double, closestToY yValue: Double, rounding: ChartDataSetRounding) -> ChartDataEntry? {
        return entries[0]
    }
    
    func entryForXValue(_ xValue: Double, closestToY yValue: Double) -> ChartDataEntry? {
        var minDiff = 1000000.0
        var entry: ChartDataEntry? = nil
        
        for e in entries {
            if e.x == xValue {
                let diff = abs(e.x - xValue)
                
                if diff < minDiff {
                    entry = e
                    minDiff = diff
                }
            }
        }
        
        return entry
    }
    
    func entriesForXValue(_ xValue: Double) -> [ChartDataEntry] {
        var ret: [ChartDataEntry] = []
        
        for e in entries {
            if e.x == xValue {
                ret.append(e)
            }
        }
        
        return ret
    }
    
    func entryIndex(x xValue: Double, closestToY yValue: Double, rounding: ChartDataSetRounding) -> Int {
        return -1
    }
    
    func entryIndex(entry e: ChartDataEntry) -> Int {
        for i in 0..<entries.count {
            if entries[i] == e {
                return i
            }
        }
        
        return -1
    }
    
    func addEntry(_ e: ChartDataEntry) -> Bool {
        entries.append(e)
        return true
    }
    
    func addEntryOrdered(_ e: ChartDataEntry) -> Bool {
        return true
    }
    
    func removeEntry(_ entry: ChartDataEntry) -> Bool {
        return true
    }
    
    func removeEntry(index: Int) -> Bool {
        return true
    }
    
    func removeEntry(x: Double) -> Bool {
        return true
    }
    
    func removeFirst() -> Bool {
        return true
    }
    
    func removeLast() -> Bool {
        return true
    }
    
    func contains(_ e: ChartDataEntry) -> Bool {
        return true
    }
    
    func clear() {
        
    }
    
    var label: String?
    
    var axisDependency: YAxis.AxisDependency = .left
    
    var valueColors: [NSUIColor] = []
    
    var colors: [NSUIColor] = [NSUIColor(cgColor: Colors.main.cgColor)]
    
    func color(atIndex: Int) -> NSUIColor {
        return mainColor
    }
    
    func resetColors() {
        
    }
    
    func addColor(_ color: NSUIColor) {
        
    }
    
    func setColor(_ color: NSUIColor) {
        mainColor = NSUIColor(cgColor: Colors.main.cgColor)
    }
    
    var highlightEnabled: Bool = false
    
    var isHighlightEnabled: Bool = false
    
    var valueFormatter: IValueFormatter? = MyValueFormatter()
    
    var needsFormatter: Bool = true
    
    var valueTextColor: NSUIColor = NSUIColor(cgColor: Colors.main.cgColor)
    
    func valueTextColorAt(_ index: Int) -> NSUIColor {
        return mainColor
    }
    
    var valueFont: NSUIFont = Fonts.sofiaSemibold()
    
    var form: Legend.Form = Legend.Form(rawValue: 0)!
    
    var formSize: CGFloat = 0
    
    var formLineWidth: CGFloat = 0
    
    var formLineDashPhase: CGFloat = 0
    
    var formLineDashLengths: [CGFloat]?
    
    var drawValuesEnabled: Bool = true
    
    var isDrawValuesEnabled: Bool = false
    
    var drawIconsEnabled: Bool = false
    
    var isDrawIconsEnabled: Bool = false
    
    var iconsOffset: CGPoint = CGPoint(x: 0, y: 0)
    
    var visible: Bool = true
    
    var isVisible: Bool = true
    
    
}

class MyShapeRenderer: IShapeRenderer {
    
    var chartHeight: CGFloat = 0.0
    var numVals = 0
    
    init(chartHeight: CGFloat, numVals: Int) {
        self.chartHeight = chartHeight
        self.numVals = numVals
    }
    
    func renderShape(context: CGContext, dataSet: IScatterChartDataSet, viewPortHandler: ViewPortHandler, point: CGPoint, color: NSUIColor) {
        
        let shapeSize = dataSet.scatterShapeSize
        let shapeHalf = shapeSize / 2.0
        
        context.setFillColor(color.cgColor)
        var rect = CGRect()
        rect.origin.x = point.x - shapeHalf
        rect.origin.y = point.y + 6
        rect.size.width = shapeSize
        rect.size.height = max(shapeSize, (chartHeight / CGFloat(numVals)) - 6)
        context.fill(rect)
    }
    
}

class MyValueFormatter: IValueFormatter {
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        return "\(Int(entry.x / 10.0))"
    }
}

class MyXAxisValueFormatter: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return  "\(Int(value))"
    }
    
    
}
