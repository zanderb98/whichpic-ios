//
//  VoteViewController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import Koloda
import fluid_slider
import SDWebImage
import Lottie

class VoteController: UIViewController {
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var loadingContainer: UIView!
    
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var slider: Slider!
    @IBOutlet weak var notesCollection: UICollectionView!
    
    private var pics: [Pic] = []
    
    var selectedNotes: [NotesCell] = []
    
    var loadingView: AnimationView?
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = AnimationView(name: "loading3")
        loadingContainer.addSubview(loadingView!)
        loadingView!.frame = loadingContainer.bounds
        loadingView!.loopMode = .loop
        loadingView!.contentScaleFactor = 3
        loadingView!.play()
        
        let flowLayout = LeftAlignedCollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        notesCollection.collectionViewLayout = flowLayout
        notesCollection.dataSource = self
        notesCollection.register(UINib(nibName: "NotesCell", bundle: nil), forCellWithReuseIdentifier: "NotesCell")

        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        PicManager.getPics { (newPics) in
            self.pics = newPics
            self.kolodaView.resetCurrentCardIndex()
        }
        
        setupSlider()
        setupTimer()
    }
    
    func setupSlider() {
        let labelTextAttributes: [NSAttributedString.Key : Any] = [.font: UIFont.systemFont(ofSize: 12, weight: .bold), .foregroundColor: UIColor.white]
        
        slider.attributedTextForFraction = { fraction in
            let formatter = NumberFormatter()
            formatter.maximumIntegerDigits = 2
            formatter.minimumIntegerDigits = 1
            formatter.maximumFractionDigits = 1
            let string = formatter.string(from: (fraction * 10) as NSNumber) ?? ""
            return NSAttributedString(string: string, attributes: [.font: UIFont.systemFont(ofSize: 12, weight: .bold), .foregroundColor: UIColor.black])
        }
        
        slider.setMinimumLabelAttributedText(NSAttributedString(string: "0", attributes: labelTextAttributes))
        slider.setMaximumLabelAttributedText(NSAttributedString(string: "10", attributes: labelTextAttributes))
        slider.fraction = 0.5
        slider.shadowOffset = CGSize(width: 0, height: 10)
        slider.shadowBlur = 5
        slider.shadowColor = UIColor(white: 0, alpha: 0.1)
        slider.contentViewColor = Colors.main
        slider.valueViewColor = .white
        
        slider.didBeginTracking = { [weak self] _ in
            if let vc = self {
                Util.fadeOut(views: [vc.sliderLabel], duration: 0.11)
            }
        }
        slider.didEndTracking = { [weak self] _ in
            if let vc = self {
                Util.fadeIn(views: [vc.sliderLabel], duration: 0.11)
            }
        }
    }
    
    func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (_) in
            if self.pics.isEmpty {
                PicManager.getPics { (newPics) in
                    self.pics = newPics
                    self.kolodaView.resetCurrentCardIndex()
                }
            }
        })
    }
    
    func votePressed() {
        if !Reachability.isConnectedToNetwork() {
            Util.defaultAlert(self, title: Constants.NoConnectionTitle, message: Constants.NoConnectionMessage)
            return
        }
        
        if kolodaView.currentCardIndex > pics.count - 1 {
            return
        }
        
        uploadVote()
        kolodaView.swipe(slider.fraction >= 0.5 ? .right : .left, force: true)
        
        if kolodaView.currentCardIndex >= kolodaView.countOfCards - 4 {
            PicManager.getPics { (newPics) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    if !newPics.isEmpty {
                        self.pics = self.pics[self.kolodaView.currentCardIndex..<self.pics.count] + newPics
                        self.kolodaView.resetCurrentCardIndex()
                    }
                }
            }
        }
        
        resetUIAfterVote()
    }
    
    func uploadVote() {
        let pic = pics[kolodaView.currentCardIndex]
        
        var picNotes: [String] = []
        
        for note in selectedNotes {
            if let noteText = note.note.text {
                picNotes.append(noteText)
            }
        } 
        
        let vote = slider.fraction * 10
        VoteManager.uploadVote(pic.id, score: vote, notes: picNotes)
        updateKarma()
    }
    
    func updateKarma() {
        let realm = Util.getRealm()
        
        if let user = realm.objects(User.self).first {
            try! realm.write {
                user.karma += 0.02
                
                ContainerController.instance?.karmaUpdated()
            }
        }
    }
    
    func resetUIAfterVote() {
        for note in selectedNotes {
            note.deselect()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.slider.fraction = 0.5
        }
    }
    
    func hasPics() -> Bool {
        return kolodaView.currentCardIndex < pics.count
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadingView?.play()
        setupTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
}

extension VoteController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return pics.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .moderate
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let card = Bundle.main.loadNibNamed("TestCard", owner: self, options: nil)![0] as! TestCard
        card.setup(with: pics[index], vc: self)
        
        return card
    }
    
}

extension VoteController: KolodaViewDelegate {
    func koloda(_ koloda: KolodaView, shouldDragCardAt index: Int) -> Bool {
        return false
    }
}

extension VoteController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Config.notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotesCell", for: indexPath) as! NotesCell
        cell.setup(text: Config.notes[indexPath.row], vc: self)
        return cell
    }
}
