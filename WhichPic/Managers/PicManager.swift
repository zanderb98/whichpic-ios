//
//  PicManager.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/27/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

class PicManager {
    static var index: Int = 0
    
    static func getPics(callback: @escaping ([Pic]) -> Void) {
        guard let user = Util.getUser() else {
            callback([])
            return
        }
        
        let params = ["UserID": user.id, "Gender": user.gender]
        
        Alamofire.request(Config.shared.getPicsURL, method: .post, parameters: params).responseJSON {
            (response) in
            
            if response.response?.statusCode != 200 {
                callback([])
                return
            }
            
            if let json = response.result.value as? [String: Any],
                let picsArr = json["Pics"] as? [[String: Any]]{
                var pics: [Pic] = []
                
                for picJSON in picsArr {
                    let pic = Pic()
                    pic.setup(json: picJSON)
                    pics.append(pic)
                }

                print("Retrieved \(pics.count) pics")
                callback(pics)
            } else {
                callback([])
            }
        }
    }
    
    static func getMyPics(user: User, callback: @escaping () -> Void) {
        let group = DispatchGroup()

        for pic in user.myPics {
            if pic.image != nil {
                continue
            }
            
            group.enter()
            Alamofire.request(pic.imageURL).responseImage { response in
                if let image = response.result.value {
                    pic.setImage(image: image)
                }
                
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            callback()
        }
    }
    
    static func uploadPic(_ image: UIImage, votedOnBy: String, notes: [String], testType: String, callback: @escaping ((Pic?) -> Void)) {
        guard let user = Util.getUser() else {
            callback(nil)
            return
        }
        
        guard let compressed = image.resizeWithWidth(width: UIScreen.main.bounds.width) else {
            callback(nil)
            return
        }
        
        let params: [String: Any] = [
            "UserID": user.id,
            "VotedOnBy": votedOnBy,
            "TestType": testType,
            "Notes": notes
        ]
        
        Alamofire.request(Config.shared.uploadPicURL, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON {
            (response) in
            
            if response.response?.statusCode != 200 {
                callback(nil)
                return
            }
            
            if let json = response.result.value as? [String: Any] {
                let pic = Pic()
                pic.setup(json: json, img: compressed)
                
                self.uploadImage(compressed, userID: user.id, pic: pic, callback: callback)
            }
        }
    }
    
    static func uploadImage(_ image: UIImage, userID: String, pic: Pic, callback: @escaping ((Pic?) -> Void)) {
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        
        let params = [
            "UserID": userID,
            "PicID": pic.id
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = image.pngData() {
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: Config.shared.uploadImageURL, method: .post, headers: headers) { (result) in
            switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        if let json = response.result.value as? [String: Any],
                            let imageURL = json["ImageURL"] as? String {
                            
                            pic.imageURL = imageURL
                            
                            let realm = Util.getRealm()
                            guard let user = Util.getUser() else {
                                callback(nil)
                                return
                            }
                            
                            realm.beginWrite()
                           
                            user.myPics.append(pic)
                            
                            do {
                                try realm.commitWrite()
                            } catch {
                                callback(nil)
                                return
                            }
                            
                            callback(pic)
                        } else {
                            callback(nil)
                        }
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    callback(nil)
            }
        }
    }
}
