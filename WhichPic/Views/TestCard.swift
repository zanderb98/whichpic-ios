//
//  TestCard.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class TestCard: UIView, UICollectionViewDataSource {

    @IBOutlet weak var card: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pic: Pic?
    var vc: UIViewController?
    
    override func awakeFromNib() {
        card.setCornerRadius(radius: 10)
        image.setCornerRadius(radius: 10)
        card.addDropShadow()
        
        
        let flowLayout = LeftAlignedCollectionViewFlowLayout()
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        
        collectionView.collectionViewLayout = flowLayout
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "NotesCell", bundle: nil), forCellWithReuseIdentifier: "NotesCell")
        
        collectionView.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func setup(with: Pic, vc: UIViewController) {
        self.pic = with
        self.vc = vc
        
        if let img = pic!.image {
            image?.image = img
        } else {
            image?.sd_setImage(with: URL(string: pic!.imageURL), placeholderImage: UIImage(named: "empty_profile_picture"), completed: nil)
        }
        
        collectionView.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.getConstraintByID(id: "height")?.constant = collectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pic == nil ? 0 : pic!.notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotesCell", for: indexPath) as! NotesCell
        let note = pic!.notes[indexPath.row]
        cell.isVoteNote = true
        cell.selectable = false
        cell.setup(text: note.note, vc: vc!)
        return cell
    }
    
}
