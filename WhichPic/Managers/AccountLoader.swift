//
//  AccountLoader.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/7/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift
import FacebookCore
import FBSDKLoginKit

class AccountLoader {
    static func loginLoadFB(accessToken: AccessToken, complete: @escaping ((Bool) -> Void), failed: @escaping (() -> Void)) {
        let dispatchGroup = DispatchGroup()
        let user = User()
        
        var firstLogin = false
        
        dispatchGroup.enter()
        UserManager.loginFB(accessToken: accessToken, user: user, failed: failed) { (first) in
            firstLogin = first
            dispatchGroup.leave()
        }
        
        if let url = URL(string: "https://graph.facebook.com/\(accessToken.userID)/picture?width=" + String(Int(UIScreen.main.bounds.width))) {
            
            dispatchGroup.enter()
            UserManager.loadProfilePicture(url: url, user: user, failed: failed) {
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            PicManager.getMyPics(user: user, callback: {
                let realm = Util.getRealm()
                
                try! realm.write {
                    realm.add(user)
                }
                
                complete(firstLogin)
            })
        }
    }
    
    static func loginLoadGoogle(idToken: String, accessToken: String, profilePicURL: URL?, complete: @escaping ((Bool) -> Void), failed: @escaping (() -> Void)) {
        let dispatchGroup = DispatchGroup()
        let user = User()
        
        var firstLogin = false
        
        dispatchGroup.enter()
        UserManager.loginGoogle(idToken: idToken, accessToken: accessToken, user: user, failed: failed) { (first) in
            firstLogin = first
            dispatchGroup.leave()
        }
        
        if let url = profilePicURL {
            
            dispatchGroup.enter()
            UserManager.loadProfilePicture(url: url, user: user, failed: failed) {
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            PicManager.getMyPics(user: user, callback: {
                let realm = Util.getRealm()
                
                try! realm.write {
                    realm.add(user)
                }
                
                complete(firstLogin)
            })
        }
    }
}
