//
//  Fonts.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

class Fonts {
    static func sofiaUltraLight(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-UltraLight", size: size)!
    }
    
    static func sofiaExtraLight(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-ExtraLight", size: size)!
    }
    
    static func sofiaLight(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-Light", size: size)!
    }
    
    static func sofiaRegular(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01-Regular", size: size)!
    }
    
    static func sofiaMedium(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-Medium", size: size)!
    }
    
    static func sofiaSemibold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-SemiBold", size: size)!
    }
    
    static func sofiaBold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "SofiaW01SC-Bold", size: size)!
    }
}
