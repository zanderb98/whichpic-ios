//
//  MyPicCell.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/30/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit
import Hero

class MyPicCell: UICollectionViewCell {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var scoreBarBack: UIView!
    @IBOutlet weak var scoreBarFront: UIView!
    @IBOutlet weak var numNotes: UILabel!
    
    @IBOutlet weak var cardLeading: NSLayoutConstraint!
    @IBOutlet weak var cardTrailing: NSLayoutConstraint!
    
    var vc: PicsController?
    var pic: Pic?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.isUserInteractionEnabled = false
        
        card.setCornerRadius(radius: 10)
        card.addDropShadow()
        
        image.setCornerRadius(radius: 10)
        image.addDropShadow()
        
        scoreBarBack.setCornerRadius(radius: scoreBarBack.frame.height / 2)
        scoreBarFront.setCornerRadius(radius: scoreBarBack.frame.height / 2)
        
    }
    
    func setup(pic: Pic, vc: PicsController, index: Int, isLast: Bool = false) {
        self.vc = vc
        self.pic = pic
        
        let isFirst = index < 2
        let last = isLast && !isFirst
        
        topView.getConstraintByID(id: "height")?.constant = isFirst ? 105 : 0
        bottomView.getConstraintByID(id: "height")?.constant = last ? 240 : 0
        
        cardLeading.constant = index % 2 == 0 ? -8 : -4
        cardTrailing.constant = index % 2 == 0 ? -4 : -8
        
        numNotes.text = "\(pic.getVoteNotes().count)"
        scoreBarFront.getConstraintByID(id: "width")?.constant = scoreBarBack.frame.width * pic.score / 10.0
        
        if let img = pic.image {
            image.image = img
            image.getConstraintByID(id: "imageHeight")?.constant = img.size.height * (image.frame.width / img.size.width)
        } else {
            image.image = UIImage(named: "empty_profile_picture")
            image.getConstraintByID(id: "imageHeight")?.constant = image.image!.size.height * (image.frame.width / image.image!.size.width)
        }
        
        layoutIfNeeded()

    }
    
    @IBAction func picPressed(_ sender: Any) {
        image.hero.id = "image"
        scoreBarFront.hero.id = "scoreBarFront"
        scoreBarBack.hero.id = "scoreBarBack"
        card.hero.id = "card"
        
        let controller = MyPicController(nibName: "MyPicController", bundle: nil)
        controller.pic = pic
        
        vc?.parent?.navigationController?.pushViewController(controller, animated: true)
    }
    
}

