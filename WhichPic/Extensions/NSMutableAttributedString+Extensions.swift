//
//  NSMutableAttributedString+Extensions.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    @discardableResult func with(_ text:String, font: UIFont? = nil, fontSize: CGFloat = 17, color: UIColor? = nil)->NSMutableAttributedString {
        var attrs:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font: font == nil ? UIFont.systemFont(ofSize: fontSize) : font!]
        
        if color != nil {
            attrs[NSAttributedString.Key.foregroundColor] = color!
        }
        
        let normal =  NSAttributedString(string: text, attributes:attrs)
        self.append(normal)
        return self
    }
    
    public func link(_ textToFind:String, linkURL:String) -> NSMutableAttributedString {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkURL, range: foundRange)
        }
        
        return self
    }
}
