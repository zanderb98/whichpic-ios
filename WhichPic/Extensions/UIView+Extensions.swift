//
//  UIView+Extensions.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/24/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func getConstraints(attr: NSLayoutConstraint.Attribute) -> [NSLayoutConstraint] {
        let searchConstraints = self.superview == nil ? constraints : self.superview!.constraints
        var ret: [NSLayoutConstraint] = []
        for constraint in searchConstraints {
            if (constraint.firstAttribute == attr || constraint.secondAttribute == attr) {
                ret.append(constraint)
            }
        }
        
        return ret
    }
    
    func getConstraintByID(id: String) -> NSLayoutConstraint? {
        let searchConstraints = self.constraints
        
        for constraint in searchConstraints {
            if (constraint.identifier == id) {
                return constraint
            }
        }
        
        return nil
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = true
    }
    
    func addDropShadow(opacity: Float = 0.3, offset: CGSize = CGSize(width: 1, height: 1), color: CGColor? = nil) {
        self.layer.shadowColor = color ?? UIColor.black.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = 5
    }
    
    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
}
