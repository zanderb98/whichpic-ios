//
//  User.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/5/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var id = "userid"
    @objc dynamic var name = ""
    @objc dynamic var data: Data? = nil
    @objc dynamic var gender = "male"
    @objc dynamic var voteOn = "female"
    @objc dynamic var karma: CGFloat = 0
    
    var myPics = List<Pic>()
    
    func setup() {
        setImage(image: UIImage(named: "empty_profile_picture")!)
        name = "John Doe"
    }
    
    func setup(json: [String: Any]) {
        self.id = json["_id"] as! String
        self.name = json["Name"] as! String
        self.gender = json["Gender"] as! String
        
        self.karma = (json["Karma"] as? CGFloat) ?? 0
        
        voteOn = "all"
        
        if gender == "male" {
            voteOn = "female"
        } else if gender == "female" {
            voteOn = "male"
        }
        
        if let myPicsArr = json["MyPics"] as? [[String: Any]] {
            myPics = List<Pic>()
            
            for picJson in myPicsArr {
                let pic = Pic()
                pic.setup(json: picJson)
                myPics.append(pic)
            }
        }
    }
    
    func setPics(pics: [Pic]) {
        myPics = List<Pic>()
        
        for pic in pics {
            myPics.append(pic)
        }
    }
    
    var image: UIImage? {
        return data == nil ? nil : UIImage(data: data!)
    }
    
    func setImage(image: UIImage) {
        data = image.pngData()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
