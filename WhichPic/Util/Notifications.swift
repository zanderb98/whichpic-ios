//
//  Notifications.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/15/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation

class Notifications {
    static let UserUpdated = Notification.Name("UserUpdated")
}
