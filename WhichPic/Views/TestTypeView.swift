//
//  TestTypeView.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/13/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class TestTypeView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var label: UILabel!
    
    var pressedCallback: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("TestTypeView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        contentView.setCornerRadius(radius: 10)
    }
    
    func setup(testName: String, priceStr: String, desc: String) {
        label.attributedText = NSMutableAttributedString().with("\(testName)", font: Fonts.sofiaSemibold(size: 16), color: UIColor(hex: "333333")).with(" - " + priceStr + " ", font: Fonts.sofiaLight(size: 16), color: UIColor(hex: "333333")).with(desc, font: Fonts.sofiaRegular(size: 16), color: UIColor(hex: "AAAAAA"))
    }
    
    func setSelected(isSelected: Bool, duration: TimeInterval  = 0.15 ) {
        UIView.animate(withDuration: duration) {
            self.contentView.addDropShadow(opacity: isSelected ? 0.3 : 0)
            self.contentView.backgroundColor = isSelected ? UIColor.white : UIColor(hex: "F2F2F2")
            self.label.alpha = isSelected ? 1 : 0.6
        }
    }
    
    @IBAction func pressed(_ sender: Any) {
        pressedCallback?()
    }
    
}
