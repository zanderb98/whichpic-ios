//
//  ResizingCollectionView.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/4/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

class ResizingCollectionView: UICollectionView {
    override func reloadData() {
        super.reloadData()
        self.collectionViewLayout.invalidationContext(forBoundsChange: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.frame.width, height: 1000)))
        self.getConstraintByID(id: "collectionHeight")?.constant = self.collectionViewLayout.collectionViewContentSize.height
    }
    
//    override var intrinsicContentSize: CGSize {
//        return self.collectionViewLayout.collectionViewContentSize
//    }
}
