//
//  UIColor+Extensions.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/25/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import  UIKit

extension UIColor {
    convenience init(hex: String) {
        var hexStr = hex
        if hex.contains("#") {
            hexStr = hex.replacingOccurrences(of: "#", with: "")
        }
        
        let scanner = Scanner(string: hexStr)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
