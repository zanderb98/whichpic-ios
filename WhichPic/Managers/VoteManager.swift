//
//  VoteManager.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/27/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class VoteManager {
    
    static func uploadVote(_ picID: String, score: CGFloat, notes: [String]) {
        guard let user = Util.getUser() else {
            return
        }
        
        let params: [String: Any] = ["UserID": user.id, "PicID": picID, "Score": score, "Notes": notes]
        Alamofire.request(Config.shared.voteURL, method: .post, parameters: params)
    }
    
}
