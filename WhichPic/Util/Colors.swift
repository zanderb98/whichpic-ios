//
//  Colors.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/25/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    static let main = UIColor(hex: "FF828F")
}
