//
//  PicsController.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 6/27/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import UIKit

class PicsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var noPics: UILabel!
    @IBOutlet weak var newPicButton: UIView!
    @IBOutlet private var collectionView: UICollectionView!
    var pics: [Pic] = []
    
    var cells: [IndexPath: MyPicCell] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
                
        let layout = PinterestLayout()
        layout.delegate = self
        
        newPicButton.setCornerRadius(radius: newPicButton.frame.height / 2)
        newPicButton.addDropShadow()
        
        
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
            parent?.automaticallyAdjustsScrollViewInsets = false
            parent?.navigationController?.automaticallyAdjustsScrollViewInsets = false
        }
        
        collectionView.collectionViewLayout = layout

        collectionView.register(UINib(nibName: "MyPicCell", bundle: nil), forCellWithReuseIdentifier: "myPicCell")
        
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.dataSource = self
        
        if let user = Util.getUser() {
            for pic in user.myPics {
                pics.append(pic)
            }
            
            pics.sort { (a, b) -> Bool in
                if a.createdAt != nil && b.createdAt != nil {
                    return a.createdAt! > b.createdAt!
                }
                
                return false
            }
            
            noPics.isHidden = !pics.isEmpty
        }
    }
    
    func addNewPic(pic: Pic) {
        pics.insert(pic, at: 0)
        collectionView.reloadData()
        
        noPics.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            let layout = PinterestLayout()
            layout.delegate = self
            self.collectionView.collectionViewLayout = layout
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myPicCell", for: indexPath) as! MyPicCell
        
        cell.setup(pic: pics[indexPath.row], vc: self, index: indexPath.row, isLast: indexPath.row == pics.count - 1)
        
        cells[indexPath] = cell
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        for (_, cell) in cells {
            cell.image.hero.id = ""
            cell.scoreBarBack.hero.id = ""
            cell.scoreBarFront.hero.id = ""
            cell.card.hero.id = ""
        }
    }
    
    @IBAction func newPicPressed(_ sender: Any) {
        let newPicsVC = NewPicController(nibName: "NewPicController", bundle: nil)
        
        newPicsVC.newPicAdded = {
            (pic) in
            self.addNewPic(pic: pic)
        }
        
        self.parent?.navigationController?.pushViewController(newPicsVC
            , animated: true)
    }
    
    
}

extension PicsController: PinterestLayoutDelegate
{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        
        let pic = pics[indexPath.row]
        
        var padding = 0
        
        if indexPath.row <= 1 {
            padding = 105
        } else if indexPath.row == pics.count - 1 {
            padding = 240
        }
        
        let img = pic.image ?? UIImage(named: "empty_profile_picture")!
        
        let cellheight = img.size.height * (UIScreen.main.bounds.width / 2 - 42) / img.size.width + 34
        return cellheight + CGFloat(padding)
    }
}
