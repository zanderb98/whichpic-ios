//
//  Config.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation

class Config {
    #if DEBUG
    let debug = true
    #else
    let debug = false
    #endif
    
    static let shared = Config()
    
    static let karmaTestDesc = "Vote on other peoples pics to get votes. Slower than paid tests."
    static let roughTestDesc = "Rough test with 15 votes."
    static let standardTestDesc = "Standard test with 45 votes."
    static let preciseTestDesc = "Precise test with 100 votes."
    
    static let notes: [String] = ["smile 👍", "outfit 👍", "background 👍", "awkward", "intense", "forced", "sad", "young", "uncomfortable"]
    
    let googleClientID = "558996123196-arcqqtklf677r5rqfssi3d8rqjeuoa8s.apps.googleusercontent.com"
    let googleURLScheme = "com.googleusercontent.apps.558996123196-arcqqtklf677r5rqfssi3d8rqjeuoa8s"

    
    var baseURL: String {
        return "https://whichpic.herokuapp.com"
    }
    
    
    //~=~=~=~=~=~=~=~=~
    //User URLs
    //~=~=~=~=~=~=~=~=~
    
    var loginURL: String {
        return baseURL + "/users/login"
    }
    
    var updateUserURL: String {
        return baseURL + "/users/update"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Pic URLs
    //~=~=~=~=~=~=~=~=~
    
    var uploadPicURL: String {
        return baseURL + "/uploads/pic"
    }
    
    var uploadImageURL: String {
        return baseURL + "/uploads/image"
    }
    
    var getPicsURL: String {
        return baseURL + "/pics/get"
    }
    
    var voteURL: String {
        return baseURL + "/pics/vote"
    }
    
    //~=~=~=~=~=~=~=~=~
    //Devices URLs
    //~=~=~=~=~=~=~=~=~
    
    var registerDeviceURL: String {
        return baseURL + "/devices/register"
    }

}

