//
//  Note.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift

class Note: Object {
    @objc dynamic var note: String = ""
    @objc dynamic var date: Date?
}
