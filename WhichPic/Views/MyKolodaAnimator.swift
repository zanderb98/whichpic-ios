//
//  MyKolodaAnimator.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/2/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import Koloda

class MyKolodaAnimator: KolodaViewAnimator {
    override func animateAppearanceWithCompletion(_ completion: KolodaViewAnimator.AnimationCompletionBlock) {
        
        self.koloda?.alpha = 1.0
        
        // custom extension method to return my CardView
        let cards = self.koloda!.cards().reversed()
        for (index, card) in cards.enumerated() {
            let delay = Double(index) * 0.2
            card.display(delay: delay)
        }
        
        completion?(true)
    }
}
