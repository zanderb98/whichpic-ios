//
//  Vote.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift

class Vote: Object {
    @objc dynamic var score: CGFloat = 0
    @objc dynamic var date: Date?
    
    var notes = List<Note>()
    
    func setup(json: [String: Any]) {
        score = CGFloat((json["score"] as? Double) ?? 0)
        date = Date(timeIntervalSince1970: (json["date"] as? Double) ?? 0)
        
        if let notesArr = json["notes"] as? [String] {
            notes = List<Note>()
            
            for str in notesArr {
                let note = Note()
                note.note = str
                notes.append(note)
            }
        }
    }
}
