//
//  Constants.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/7/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation

class Constants {
    static let NoConnectionTitle = "No Connection"
    static let NoConnectionMessage = "Check your internet connection and try again."
}
