//
//  UserManager.swift
//  WhichPic
//
//  Created by Zander Bobronnikov on 7/3/19.
//  Copyright © 2019 Zander Bobronnikov. All rights reserved.
//

import Foundation
import RealmSwift
import FBSDKLoginKit
import Alamofire

class UserManager {
    
    static func updateUser(_ old: User) {
        let params = ["UserID": old.id]
        
        Alamofire.request(Config.shared.updateUserURL, method: .post, parameters: params).responseJSON { (response) in
            
            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                
                let user = User()
                
                user.setup(json: userJson)
                user.data = old.data
                
                for oldPic in old.myPics {
                    for pic in user.myPics {
                        if pic.id == oldPic.id {
                            pic.data = oldPic.data
                            break
                        }
                    }
                }
                
                PicManager.getMyPics(user: user, callback: {
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(user, update: .all)
                        NotificationCenter.default.post(Notification(name: Notifications.UserUpdated))
                    }
                })
            }
        }
    }
    
    static func loadProfilePicture(url: URL, user: User,
                                   failed: @escaping (() -> Void),
                                   complete: (() -> Void)) {
        
        guard let data = try? Data(contentsOf: url) else {
            failed()
            return
        }
        
        user.data = data
        complete()
    }
    
    static func loginFB(accessToken: AccessToken, user: User,
                        failed: @escaping (() -> Void),
                        complete: @escaping ((Bool) -> Void)) {
                
        let params = ["FBAccessToken": accessToken.tokenString,
                      "FBUserID": accessToken.userID,
                      "Platform": "iOS",
                      "Type": "facebook",
                      "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]

        Alamofire.request(Config.shared.loginURL, method: .post, parameters: params).responseJSON { (response) in

            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                user.setup(json: userJson)
                complete(userJson["FirstLogin"] as! Bool)
            } else {
                failed()
            }
        }
    }
    
    static func loginGoogle(idToken: String, accessToken: String, user: User,
                            failed: @escaping (() -> Void),
                            complete: @escaping ((Bool) -> Void)) {
        let params = ["GoogleIDToken": idToken,
                      "GoogleAccessToken": accessToken,
                      "Type": "google",
                      "Platform": "iOS",
                      "DeviceToken": UserDefaults.standard.string(forKey: "DeviceToken") ?? ""]

        Alamofire.request(Config.shared.loginURL, method: .post, parameters: params).responseJSON { (response) in

            if let json = response.result.value as? [String: Any],
                let userJson = json["User"] as? [String: Any] {
                user.setup(json: userJson)
                complete(userJson["FirstLogin"] as! Bool)
            } else {
                failed()
            }
        }
    }
    
}
